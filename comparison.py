import os
import json
import glob
import os.path as osp
import numpy as np
import math
import pandas as pd

from pycocotools.coco import COCO

'''
Comparison script for AlphaPose, MMPose, Detectron2, OpenPose data
@author: Matěj Mísař
@contact: misarmat@fel.cvut.cz
@date: 5.5.2022
'''

alphapose_path = '/home/matej/Documents/project/AlphaPose/examples/res/data/'
mmpose_path = '/home/matej/Documents/project/mmpose/out_keypoints/data/'
detectron_path = '/home/matej/Documents/project/detectron2/out_key/'
openpose_path = '/home/matej/Documents/project/OpenPose/converted/'
annotation_file = '/home/matej/Documents/project/coco/person_keypoints_val2017.json'

coco = COCO(annotation_file)


def get_annotation(coco, filename):
    temp = []
    filename = osp.basename(filename)[:-5]
    for i in range(len(filename)):
        if filename[0] == '0':
            filename = filename[1:]
        else:
            break
    special_id = coco.getAnnIds(int(filename))
    for i in range(len(coco.loadAnns(special_id))):
        temp.append([coco.loadAnns(special_id)[i]['keypoints'],
                    coco.loadAnns(special_id)[i]['bbox']])
    return temp


""" return_tuple = [[center1, [keypoints1]], [center2, [keypoints2]] ...] """


def structuration(keypoints, gt=False):
    temp = []
    if not gt:
        for key in keypoints.keys():
            temp.append([get_center(keypoints[key]['keypoints']),
                        keypoints[key]['keypoints']])
    else:
        for i in range(len(keypoints)):
            temp.append([get_center(keypoints[i][0]), keypoints[i]])
    temp.sort()
    return temp


""" count "Center" from head keypoints"""


def get_center(keypoints):  # [x1, y1, prob1, ..., x5, y5, prob5]
    count = 0
    cent_x = 0
    cent_y = 0
    for i in range(0, 15, 3):
        if keypoints[i+2] == 0:
            continue
        else:
            cent_x += keypoints[i]
            cent_y += keypoints[i+1]
            count += 1
    if count == 0:
        count += 1
    return [cent_x/count, cent_y/count]


def binary_det(gt, alpha, mm, det, open):
    gt_per = len(gt)
    person = [len(alpha), len(mm), len(det), len(open)]
    result_extra = []
    result_miss = []
    for i, _ in enumerate(person):
        if gt_per == person[i]:  # right amount of detected people
            result_num = 0
            result_miss.append(0)
            result_extra.append(0)
        elif gt_per > person[i]:    # missing detection
            result_num = gt_per - person[i]
            result_miss.append(result_num)
            result_extra.append(0)
        elif gt_per < person[i]:    # extra detection
            result_num = person[i] - gt_per
            result_extra.append(result_num)
            result_miss.append(0)
    return result_extra


def get_dist(A_x, B_x, A_y, B_y):
    dx = B_x - A_x
    dy = B_y - A_y
    return [math.sqrt((dx)**2 + (dy)**2), [dx, dy]]


def get_OKS(d, i, scale):
    k = np.array([.26, .25, .25, .35, .35, .79, .79, .72, .72, .62, .62,
                 1.07, 1.07, .87, .87, .89, .89])/10.0
    e = ((d[0]**2 + d[1]**2) / (2*(scale**2)*(k[i//3]**2)))
    exp = np.exp(-e)

    return exp


def count_diff(gt, detection):
    errors = []  # Tuple of errors from each detection paired with one GT + index of detected person
    for i in range(len(gt)):    # Loop through all GT people
        bboxArea = gt[i][1][1][2] * gt[i][1][1][3]
        scale = math.sqrt(bboxArea)
        for l in range(len(detection)):  # Loop through all detected people
            skip_miss = 0
            skip_label = 0
            distance_temp = 0
            e = 0
            for j in range(0, 51, 3):   # Loop through all keypoints
                # GT probability 0 = not labeled, 1 = not visible, 2 = visible
                if gt[i][1][0][j+2] == 0:
                    skip_label += 1
                    continue
                if detection[l][1][j+2] == 0 and (gt[i][1][0][j+2] == 2 or gt[i][1][0][j+2] == 1):
                    skip_miss += 1
                    continue

                temp = get_dist(gt[i][1][0][j], detection[l]
                                [1][j], gt[i][1][0][j+1], detection[l][1][j+1])
                distance_temp += temp[0]
                e += get_OKS(temp[1], j, scale)
            diagonal = get_dist(gt[i][1][1][0], gt[i][1][1][0] + gt[i]
                                [1][1][2], gt[i][1][1][1], gt[i][1][1][1] + gt[i][1][1][3])[0]

            # Adds all distance errors, divide at the end by number of keypoints
            if skip_label + skip_miss != 17:
                mean_error = distance_temp / (17 - skip_miss - skip_label)
                percentage_error = mean_error / diagonal
                oks_mean = e / (17 - skip_miss - skip_label)

                if percentage_error > 0.3:    # If percentage error > 30%, check if atleast 1 keypoint in bbox
                    if not bb_check(gt[i][1][1], detection[l][1]):
                        continue

                #   GT person idx, Detection person idx, mean_err, % error, no keypoints check
                errors.append(
                    [i, l, mean_error, percentage_error, False, skip_miss, oks_mean])
            else:
                if bb_check(gt[i][1][1], detection[l][1]):
                    errors.append([i, l, 0, 0, True, skip_miss, 0])

    errors.sort(key=lambda x: (x[4], x[2]))

    # match GT with detection with lowest absolute error
    used_anno = []
    used_det = []
    total_error = 0
    total_percentage = 0
    person_count = 0
    key_miss = 0
    oks = 0
    for k in range(len(errors)):
        if errors[k][0] in used_anno or errors[k][1] in used_det:
            continue
        elif errors[k][4] == False:
            used_anno.append(errors[k][0])
            used_det.append(errors[k][1])
            total_error += errors[k][2]
            total_percentage += errors[k][3]
            person_count += 1
            key_miss += errors[k][5]
            oks += errors[k][6]
        if errors[k][4] == True and errors[k][1] not in used_det and errors[k][0] not in used_anno:
            used_anno.append(errors[k][0])
            used_det.append(errors[k][1])
    if person_count == 0:
        person_count = 1

    total_error = total_error / person_count
    total_percentage = total_percentage / person_count
    oks = oks / person_count

    # Return Missed ppl, AbsoluteError, RelativeError, OpenPose KeyMiss
    return [(len(gt) - len(used_anno)), total_error, total_percentage, key_miss, oks]


"""check if atleast one keypoint is inside of bbox"""


def bb_check(bbox, keypoints):
    bbmin_x = bbox[0]
    bbmin_y = bbox[1]
    bbmax_x = bbmin_x + bbox[2]
    bbmax_y = bbmin_y + bbox[3]
    for i in range(0, 51, 3):
        if keypoints[i] <= bbmax_x and keypoints[i] >= bbmin_x and keypoints[i+1] <= bbmax_y and keypoints[i+1] >= bbmin_y:
            return True
    return False


def compare_all(gt, alpha, mm, det2, open):
    keypoints = [alpha, mm, det2, open]
    result = []
    for i, _ in enumerate(keypoints):
        result.append(count_diff(gt, keypoints[i]))
    return result


def save_data(filenames, people_num, A_extra, A_miss, A_absError, A_perError, M_extra, M_miss, M_absError, M_perError,
              D_extra, D_miss, D_absError, D_perError, O_extra, O_miss, O_absError, O_perError, O_keyMiss, alpha_OKS, mm_OKS, det_OKS, open_OKS):

    raw_data = {'Filename': filenames, 'Total_people': people_num, 'AlphaPose_extra': A_extra, 'AlphaPose_miss': A_miss,
                'AlphaPose_AbsError': A_absError, 'AlphaPose_RelativeError': A_perError, 'AlphaPose_OKS': alpha_OKS, 'MMPose_extra': M_extra, 'MMPose_miss': M_miss,
                'MMPose_AbsError': M_absError, 'MMPose_RelativeError': M_perError, 'MMPose_OKS': mm_OKS, 'Detectron2_extra': D_extra, 'Detectron2_miss': D_miss,
                'Detectron2_AbsError': D_absError, 'Detectron2_RelativeError': D_perError, 'Detectron2_OKS': det_OKS,
                'OpenPose_extra': O_extra, 'OpenPose_miss': O_miss, 'OpenPose_AbsError': O_absError, 'OpenPose_RelativeError': O_perError, 'OpenPose_keyMiss': O_keyMiss, 'OpenPose_OKS': open_OKS}

    df = pd.DataFrame(raw_data, columns=['Filename', 'Total_people', 'AlphaPose_extra', 'AlphaPose_miss',
                                         'AlphaPose_AbsError', 'AlphaPose_RelativeError', 'AlphaPose_OKS', 'MMPose_extra', 'MMPose_miss',
                                         'MMPose_AbsError', 'MMPose_RelativeError', 'MMPose_OKS', 'Detectron2_extra', 'Detectron2_miss',
                                         'Detectron2_AbsError', 'Detectron2_RelativeError', 'Detectron2_OKS',
                                         'OpenPose_extra', 'OpenPose_miss', 'OpenPose_AbsError', 'OpenPose_RelativeError', 'OpenPose_keyMiss', 'OpenPose_OKS'])

    df.to_csv('comparison_datav2.csv', index=False)


if __name__ == '__main__':
    # AlphaPose, MMPose, Detectron2, OpenPose
    bin_people = []

    alpha_extra = []
    alpha_miss = []
    mm_extra = []
    mm_miss = []
    det_extra = []
    det_miss = []
    open_extra = []
    open_miss = []

    alpha_absError = []
    alpha_perError = []
    mm_absError = []
    mm_perError = []
    det_absError = []
    det_perError = []
    open_absError = []
    open_perError = []

    alpha_OKS = []
    det_OKS = []
    mm_OKS = []
    open_OKS = []

    filenames = []

    open_keyMiss = []

    for filename in glob.glob(os.path.join(mmpose_path, '*.json')):
        print(osp.basename(filename))
        # filename = '/home/matej/Documents/project/mmpose/out_keypoints/data/000000125572.json'
        try:
            with open(os.path.join(os.getcwd(), alphapose_path + osp.basename(filename)), 'r') as f_alpha:
                alpha_file = json.load(f_alpha)
                alpha_key = structuration(alpha_file)
        except:
            alpha_key = []

        with open(os.path.join(os.getcwd(), filename), 'r') as f_mm:
            mm_file = json.load(f_mm)
            mm_key = structuration(mm_file)

        with open(os.path.join(os.getcwd(), detectron_path + osp.basename(filename)), 'r') as f_det:
            det_file = json.load(f_det)
            det_key = structuration(det_file)

        with open(os.path.join(os.getcwd(), openpose_path + osp.basename(filename))[:-5] + '_keypoints.json', 'r') as f_open:
            open_file = json.load(f_open)
            open_key = structuration(open_file)

        ann_gt = get_annotation(coco, filename)
        gt_key = structuration(ann_gt, True)

        #   int tuple [AlphaPose, MMPose, Detectron2, OpenPose]
        extra = binary_det(gt_key, alpha_key, mm_key, det_key, open_key)

        #   compare_stats = [ A[Miss, AbsError, %Error, kptsMiss, OKS], M[Miss, AbsError, ..]]
        compare_stats = compare_all(
            gt_key, alpha_key, mm_key, det_key, open_key)

        """ append usefull data to extract them in predefined format"""
        miss = [compare_stats[0][0], compare_stats[1][0],
                compare_stats[2][0], compare_stats[3][0]]
        filenames.append(osp.basename(filename))
        bin_people.append(len(gt_key))
        alpha_absError.append(compare_stats[0][1])
        alpha_perError.append(compare_stats[0][2])
        mm_absError.append(compare_stats[1][1])
        mm_perError.append(compare_stats[1][2])
        det_absError.append(compare_stats[2][1])
        det_perError.append(compare_stats[2][2])
        open_absError.append(compare_stats[3][1])
        open_perError.append(compare_stats[3][2])

        alpha_extra.append(extra[0])
        mm_extra.append(extra[1])
        det_extra.append(extra[2])
        open_extra.append(extra[3])

        alpha_miss.append(miss[0])
        mm_miss.append(miss[1])
        det_miss.append(miss[2])
        open_miss.append(miss[3])

        open_keyMiss.append(compare_stats[3][3])

        alpha_OKS.append(compare_stats[0][4])
        mm_OKS.append(compare_stats[1][4])
        det_OKS.append(compare_stats[2][4])
        open_OKS.append(compare_stats[3][4])

    save_data(filenames, bin_people, alpha_extra, alpha_miss, alpha_absError, alpha_perError,
              mm_extra, mm_miss, mm_absError, mm_perError, det_extra, det_miss, det_absError,
              det_perError, open_extra, open_miss, open_absError, open_perError, open_keyMiss,
              alpha_OKS, mm_OKS, det_OKS, open_OKS)
