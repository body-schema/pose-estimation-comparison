import json
import os

f = open('alphapose-results.json')
data = json.load(f)
f.close()
output = {}
index = 0
temp = 'default'
for i in range(len(data)):
    if temp != data[i]['image_id'] or i == len(data) - 1:
        with open(os.path.join('data/' + temp[:-4] + '.json'), 'w') as json_file:
            json_file.write(json.dumps(output))
        json_file.close()
        output = {}
        index = 0
        temp = data[i]['image_id']
    output[index] = {"keypoints": data[i]['keypoints']}
    index += 1
os.remove('/home/matej/Documents/project/AlphaPose/examples/res/data/def.json')