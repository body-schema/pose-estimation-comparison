import json
import os
import glob

path = 'txt/'
for filename in glob.glob(os.path.join(path, '*.txt')):
    with open(os.path.join(os.getcwd(), filename), 'r') as f:  # open in readonly mode
       # do your stuff
        data = f.read()
        f.close()
        cleaned = data.replace('dtype=float32', '')
        cleaned = cleaned.replace('array', '')
        ev_data = eval(cleaned)
        result = {}
        temp = []
        for l in range(len(ev_data)):
            for i in range(len(ev_data[l]['keypoints'][0])):
                temp += ev_data[l]['keypoints'][0][i]
            result[l] = {"keypoints": temp}
            temp = []
        with open(os.path.join('data/' + filename[4:-4] + '.json'), 'w') as json_file:
            json_file.write(json.dumps(result))
        json_file.close()
